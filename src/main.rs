use anyhow::{bail, Context as _, Result};
use argh::FromArgs;
use tera::{Context, Tera};
use walkdir::WalkDir;

use std::{env, fs, path::Path};

#[derive(FromArgs)]
/// stupidly simple scaffolding project
struct Args {
    #[argh(positional)]
    /// which template to copy
    templates: Vec<String>,

    #[argh(switch, short = 'f')]
    /// whether to overwrite files
    force: bool,
}

fn main() -> Result<()> {
    let args: Args = argh::from_env();

    let repository = env::var("INC_REPO")
        .context("Variable $INC_REPO not set. It needs to be set. Refer to inc's README.")?;

    let repository = Path::new(&repository);

    if args.templates.is_empty() {
        println!("Templates:");
        for entry in repository
            .read_dir()
            .context("error reading $INC_REPO's files")?
        {
            let file_name = entry?.file_name();
            let file_name = file_name.to_str().context("path is not utf-8")?;
            println!("- {}", file_name);
        }
        return Ok(());
    }

    let mut stdlib = Context::new();
    {
        let current_dir = std::env::current_dir().context("failed to get current directory")?;
        let dirname = current_dir
            .file_name()
            .context("failed to get current directory's filename")?
            .to_str()
            .context("cwd's filename is not utf-8")?;
        stdlib.insert("dirname", dirname);
    }
    {
        let config = git2::Config::open_default().context("failed to get git config")?;
        let name = config
            .get_string("user.name")
            .context("failed to get git username")?;
        stdlib.insert("user", &name);
    }

    for template in &args.templates {
        let template_path = repository.join(&template);

        let glob = template_path.join("**/*");
        let glob = glob.to_str().context("path is not utf-8")?;
        let tera = Tera::new(glob)?;

        for entry in WalkDir::new(&template_path).follow_links(false) {
            let entry = entry.context("error reading template file")?;

            let path = entry.path();
            let relative = path
                .strip_prefix(&template_path)
                .context("was not prefix of template directory")?;

            if entry.file_type().is_dir() {
                fs::create_dir_all(relative).context("creating directory")?;
            } else {
                if relative.exists() && !args.force {
                    bail!("Aaah, file {} exists. Use -f to force.", relative.display());
                }

                let name = relative.to_str().context("path is not utf-8")?;
                let generated = tera.render(name, &stdlib)?;

                fs::write(relative, generated).context("failed to write file")?;
            }
        }
    }

    Ok(())
}
