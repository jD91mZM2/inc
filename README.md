# Stupidly simple scaffolding project

I hate scaffolding projects.

- I've created [scaff](https://gitlab.com/jD91mZM2/scaff/) to try to mitigate
  the problem. Scaff is a bloated project and I shouldn't have created it.
- Just today, as I'm first writing this, I created
  [old](https://gitlab.com/jD91mZM2/old), an even more bloated scaffolding
  project.

The problem is, these projects try to solve too much. In practice, all I want
from my scaffolding project is to

1. Be able to quickly update my repository.
2. Be able to dump files in my current directory.

So I decided to make a program I'd actually be comfortable relying on daily,
one that doesn't try to do too much.

Features:
- Currently under 100 lines of code

## How does this compare to `X`?

W O A H, there are alternatives? Maybe I should use that instead.
