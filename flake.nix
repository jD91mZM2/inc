{
  description = "Stupidly simple scaffolding";

  inputs = {
    utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nmattia/naersk";
  };

  outputs = { self, nixpkgs, utils, naersk }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
        naersk-lib = naersk.lib."${system}";

        nativeBuildInputs = with pkgs; [ pkg-config ];
        buildInputs = with pkgs; [ openssl ];
      in
      rec {
        # `nix build`
        packages.inc = naersk-lib.buildPackage {
          name = "inc";
          src = ./.;

          inherit nativeBuildInputs buildInputs;
        };
        defaultPackage = packages.inc;

        # `nix run`
        apps.inc = utils.lib.mkApp {
          drv = packages.inc;
        };
        defaultApp = apps.inc;

        # `nix develop`
        devShell = pkgs.mkShell {
          buildInputs = buildInputs;
          nativeBuildInputs = nativeBuildInputs ++ [ pkgs.rustc pkgs.cargo ];
        };
      });
}
